const gameContainer = document.getElementById("game");
const elements = gameContainer.children;
const numColors = 5;
let COLORS = getRandomColors(numColors);
const start = document.getElementById("start");
let score = 0;
let Matches = 0;

function best() {
    const best = document.getElementById("best")
    if (localStorage.getItem("score") == null) {} else {
        let bestScore = localStorage.getItem("score");
        let txt = "BEST SCORE:" + bestScore;
        best.textContent = txt;
    }
}
best();

function getRandomColors(num) {
    let colors = [];
    let i = 0;
    for (let i = 0; i < num; i++) {
        let r = '#' + Math.floor(Math.random() * 16777215).toString(16);
        while (colors.includes(r)) {
            r = '#' + Math.floor(Math.random() * 16777215).toString(16);
        }
        colors.push(r);
        colors.push(r);
    }
    return colors;
}
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(COLORS);
// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    updateScore(score);
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over
        newDiv.classList.add(color);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);
        newDiv.style.backgroundColor = "white";
        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}
let pre = "";
// TODO: Implement this function!
function handleCardClick(event) {
    let thisColor = event.target.classList[0]
    event.target.style.transform = "rotateY(180deg)";
    event.target.style.background = thisColor;
    if (pre == "") {
        pre = event.target;
        pre.removeEventListener("click", handleCardClick);
    } else {
        score++;
        updateScore(score);
        if (pre.classList[0] == event.target.classList[0]) {
            event.target.removeEventListener("click", handleCardClick);
            event.target.classList.add("matched");
            pre.classList.add("matched");
            Matches++;
            if (Matches == numColors) {
                setTimeout(() => {
                    restart();
                }, 1000);
            }
            console.log("match");
            pre = "";
        } else {
            disableElements()
            setTimeout(() => {
                pre.style.transform = "rotateY(360deg)"
                pre.style.background = "white";
                event.target.style.transform = "rotateY(360deg)"
                event.target.style.background = "white";
                pre.addEventListener("click", handleCardClick);
                event.target.addEventListener("click", handleCardClick);
                pre = "";
                enableElements();
            }, 1000)
        }
    }
}

function updateScore(score) {
    text = "SCORE: " + score.toString();
    document.getElementById("score").textContent = text;
}

function disableElements() {
    for (let i = 0; i < elements.length; i++) {
        elements[i].removeEventListener("click", handleCardClick);
    }
}

function enableElements() {
    for (let i = 0; i < elements.length; i++) {
        if (!elements[i].classList[1]) {
            elements[i].addEventListener("click", handleCardClick);
        }
    }
}


start.addEventListener("click", startGame);

function restart() {
    console.log(score);
    bestScore(score);
    best();
    score = 0;
    Matches = 0;
    document.getElementById("score").textContent = '';
    gameContainer.textContent = '';
    COLORS = getRandomColors(numColors);
    shuffledColors = shuffle(COLORS);
    start.textContent = "Restart";
    start.style.width = "150px";
    document.getElementById("main").style.display = "block";
}

function bestScore(s) {
    if (localStorage.getItem("score") == null) {
        localStorage.setItem("score", s);
    } else {
        let s1 = localStorage.getItem("score");
        if (s < s1) {
            localStorage.setItem("score", s);
        }
    }
}
// when the DOM loads
function startGame() {
    document.getElementById("main").style.display = "none";
    createDivsForColors(shuffledColors);
}
console.log(score);